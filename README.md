# User CRUD TypeORM + JWT

A User CRUD api for study purposes with Kenzie Academy Brasil.

# Description

To use the api, clone the project on the following link:

<https://gitlab.com/hirokz7/typeorm-crud-de-usuario-jwt>

# Installation

Open the project and install the dependencies:

`yarn install`

On this project we use Docker, so you gonna need to have docker installed and use the command above:

`docker-compose up`


# How to use

With your API Client you can use the following routes:

### Routes

**POST /users**

To create your User:

RESPONSE STATUS -> HTTP 201(CREATED)

Body:

```json
{
  "name": "Carlos",
  "isAdmin": true,
  "email": "carlos@mail.com",
  "password": "12345678"
}
```

Response:

```json
{
  "name": "Carlos",
  "email": "carlos@mail.com",
  "isAdmin": true,
  "id": "k392012a-a5ca-4e40-81cf-18ac1223011y6",
  "createdAt": "2022-02-01T00:00:00.000Z",
  "updatedAt": "2022-02-01T00:00:00.000Z"
}
```

**POST /login**

Login route.

RESPONSE STATUS -> HTTP 200(OK)

Body:

```json
{
  "email": "carlos@mail.com",
  "password": "12345678"
}
```

Response:

```json
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyRGF0YSI6eyJpZCI6ImE3ZjA0OGFiLWVhMDUtNGZmNy05YjE5LTZjYjQzN2M3M2FiZCIsIm5hbWUiOiJDYXJsb3MgbGltIiwiZW1haWwiOiJjYXJsb3NAYWRtaW4uY29tIiwiaXNBZG1pbiI6dHJ1ZSwiY3JlYXRlZEF0IjoiMjAyMi0wMi0wMVQwNDo0Mjo1MC44OTVaIiwidXBkYXRlZEF0IjoiMjAyMi0wMi0wMVQwNjo1ODowNy40MjdaIn0sImlhdCI6MTY0MzY5ODY5NSwiZXhwIjoxNjQzNzg1MDk1fQ.3MzEh1xGGFTDm21ayXtvQM3oEihMQmA6rEZeVxLZOQ4"
}
```

**GET /users**

Get of all users:

Authorization Header:

**Authorization Header with Bearer Token Needed and only Admin users have access**


RESPONSE STATUS -> HTTP 200(OK)

Body:

_No body_

Response:

```json
[
  {
    "id": "k392012a-a5ca-4e40-81cf-18ac1223011y6",
    "name": "Carlos",
    "email": "carlos@mail.com",
    "isAdmin": true,
    "createdAt": "2022-02-01T00:00:00.000Z",
    "updatedAt": "2022-02-01T00:00:00.000Z"
  },
  {
    "id": "wqdcaac-5fba-4b10-bdqd-5d80d39wq19e",
    "name": "Common User",
    "email": "common@mail.com",
    "isAdmin": false,
    "createdAt": "2022-02-01T00:00:00.000Z",
    "updatedAt": "2022-02-01T00:00:00.000Z"
  }
]
```

**GET /users/profile**

Route to get the current user

Authorization Header:

**Authorization Header with Bearer Token Needed**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

_No body_

Response:

```json
{
    "id": "k392012a-a5ca-4e40-81cf-18ac1223011y6",
    "name": "Carlos",
    "email": "carlos@mail.com",
    "isAdmin": true,
    "createdAt": "2022-02-01T00:00:00.000Z",
    "updatedAt": "2022-02-01T00:00:00.000Z"
}
```

**PATCH /users/:id**

Route to update user, you can't update isAdmin field.

**Only Admin can update all the users, common user just can update the own account**

Authorization Header:

**Authorization Header with Bearer Token Needed**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

````json
{
  "name": "Carlos Lima",
  "email": "carlos@email.com"
}

Response:

```json
{
    "id": "k392012a-a5ca-4e40-81cf-18ac1223011y6",
    "name": "Carlos Lima",
    "email": "carlos@email.com",
    "isAdmin": true,
    "createdAt": "2022-02-01T00:00:00.000Z",
    "updatedAt": "2022-02-01T11:11:11.111Z"
}
````


**DELETE /users/:id**

Route to delete a user

**Only Admin can update all the users, common user just can update the own account**

Authorization Header:

**Authorization Header with Bearer Token Needed**

RESPONSE STATUS -> HTTP 200(Ok)

Body:

_No body_

Response:

```json
{
  "message": "User deleted with success"
}
```

## Technologies

- TypeScript
- Express.js
- Docker
- TypeORM
