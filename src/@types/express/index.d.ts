declare namespace Express {
    interface Request {
        user: { userData: { 
            id: string, 
            name: string,
            email: string,
            password: string,
            isAdm: boolean,
            createdAt: string,
            updatedAt: string }}
}
}
