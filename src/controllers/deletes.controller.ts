import { Request, Response, NextFunction} from "express";
import { deleteTodos, deleteDones } from "../services/deletes.service";


export const destroy_todos = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const id = req.params.id;
        await deleteTodos(req, id)
        
        res.sendStatus(204)
    } catch(error){
        next(error);     
    }
}

export const destroy_dones = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const id = req.params.id;
        await deleteDones(req, id)
        
        res.sendStatus(204)
    } catch(error){
        next(error);     
    }
}