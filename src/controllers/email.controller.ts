import { Request, Response, NextFunction} from "express";

import { emailFromUser } from "../services/email.service";

export const sendEmail = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const email = await emailFromUser(req, next);
        
        res.status(200).json(email);
    } catch (error) {
        next(error);
    }

}