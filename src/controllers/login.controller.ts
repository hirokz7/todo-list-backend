import { NextFunction, Request, Response } from "express";
import { authenticateUser } from "../services/login.service";

export const login = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const { email, password } = req.body;
        const token = await authenticateUser(email, password);
        res.send({ token });
    } catch (error) {
        next(error)
    }
}