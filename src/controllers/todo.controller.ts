import { Request, Response, NextFunction} from "express";
import { createTodo, updateTodo, listTodos, deleteTodo, findId } from "../services/todo.service";

export const create = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const todo = await createTodo(req);
        
        res.status(201).json(todo);
    } catch (error) {
        next(error);
    }

}

export const list = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const todo = await listTodos();
    
        res.status(200).json(todo);
    } catch (error) {
        next(error)
    }
}

export const findTodo = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const id = req.params.id;
        const todo = await findId(id);
    
        res.status(200).json(todo);
    } catch (error){
        next(error)
    }
}

export const update = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const id = req.params.id;
        const userUpdated = await updateTodo(req, id)
        
        res.json(userUpdated)
    } catch(error){
        next(error);   
    }
}

export const destroy = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const id = req.params.id;
        await deleteTodo(req, id)
        
        res.sendStatus(204)
    } catch(error){
        next(error);     
    }
}