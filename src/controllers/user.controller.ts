import { Request, Response, NextFunction} from "express";

import { createUser, updateUser, listUser, deleteUser, getProfile } from "../services/user.service";

export const create = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user = await createUser(req.body);

        const { password: user_password, ...userWithoutPassword } = user
        
        res.status(201).json(userWithoutPassword);
    } catch (error) {
        next(error);
    }

}

export const list = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const users = await listUser();
    
        const listUsers = users.map(item => {
            const { password: user_password, ...userWithoutPassword} = item
            return userWithoutPassword
          })
    
        res.status(200).json(listUsers);
    } catch (error) {
        next(error)
    }
}

export const currentUser = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const user = await getProfile(req.user.userData.id);
    
        res.status(200).json(user);

    } catch (error){
        next(error)
    }
}

export const update = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const id = req.params.id;
        const userUpdated = await updateUser(req, id)
        
        res.json(userUpdated)
    } catch(error){
        next(error);   
    }
}

export const destroy = async (req: Request, res: Response, next: NextFunction) => {
    try{
        const id = req.params.id;
        const userDeleted = await deleteUser(req, id)
        
        res.status(200).json(userDeleted)
    } catch(error){
        next(error);     
    }
}