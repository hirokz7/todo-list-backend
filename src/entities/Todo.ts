import { 
    Entity, 
    Column, 
    CreateDateColumn, 
    PrimaryGeneratedColumn,
    ManyToOne, 
    UpdateDateColumn,
    BeforeInsert} from "typeorm";
import { User } from ".";

  
@Entity("buys")
export class Todo {
    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @Column()
    name!: string;

    @Column()
    isConcluded!: boolean;

    @CreateDateColumn()
    createdAt!: Date;

    @UpdateDateColumn()
    updatedAt!: Date;

    @BeforeInsert()
    async hashPassword() {
        this.isConcluded = false
    }
    
    @ManyToOne(type => User, user => user.todolist) 
    user!: User; 
}
  