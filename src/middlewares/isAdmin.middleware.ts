import jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from 'express';
import AppError from '../errors/AppError';


export const isAdmin = (req: Request, res: Response, next: NextFunction) => {
    try{
     const token = req.headers.authorization?.split(' ')[1];
 
     jwt.verify(token as string, process.env.SECRET as string, (err: any, decoded: any) => {
        if (!decoded.userData.isAdm){
            throw new AppError("Unauthorized", 401)
        }
         next();
     });
    } catch (error){
        throw new AppError("Unauthorized", 401);
    }
 }
