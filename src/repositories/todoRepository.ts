import { EntityRepository, Repository } from "typeorm";
import { Todo } from "../entities/Todo";

@EntityRepository(Todo)
class todoRepository extends Repository<Todo> {
    public async findById(id: string): Promise<Todo | undefined> {
        const user = await this.findOne({
            where: {
                id
            }
        });

        return user;
    }
}

export default todoRepository ;
