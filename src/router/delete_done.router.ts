import { Router } from "express";

import { destroy_dones } from "../controllers/deletes.controller";
import { isAuthenticated } from '../middlewares/authentication.middleware'
import { adminOrOwner } from "../middlewares/adminOrOwner.middleware";

const router = Router();

export const deleteDonesRouter = () => {
    router.delete('/:id', isAuthenticated, adminOrOwner, destroy_dones)
    return router;
}
