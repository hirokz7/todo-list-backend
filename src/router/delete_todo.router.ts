import { Router } from "express";

import { isAuthenticated } from '../middlewares/authentication.middleware'
import { destroy_todos } from "../controllers/deletes.controller";
import { adminOrOwner } from "../middlewares/adminOrOwner.middleware";

const router = Router();

export const deleteTodosRouter = () => {
    router.delete('/:id', isAuthenticated, adminOrOwner, destroy_todos)
    return router;
}
