import { Router } from "express";
import { sendEmail } from "../controllers/email.controller"

const router = Router();

export const emailRouter = () => {
    router.post('', sendEmail);
    
    return router;
}