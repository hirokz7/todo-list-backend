import { Express } from "express";
import { userRouter } from "./user.router";
import { loginRouter } from "./login.router";
import { todoRouter } from "./todo.router";
import { emailRouter } from "./email.router"
import cors from "cors"
import { deleteTodosRouter } from "./delete_todo.router";
import { deleteDonesRouter} from "./delete_done.router"

export const initializerRouter = (app: Express) => {
    app.use(cors())
    app.use('/login', loginRouter());
    app.use('/users', userRouter());
    app.use('/users/todo', todoRouter());
    app.use('/email', emailRouter());   
    app.use('/delete_todos', deleteTodosRouter());
    app.use('/delete_dones', deleteDonesRouter())
}