import { Router } from "express";

import { create, list, findTodo, update, destroy } from "../controllers/todo.controller";
import { isAuthenticated } from '../middlewares/authentication.middleware'
import { isAdmin } from "../middlewares/isAdmin.middleware";
import { adminOrOwner } from "../middlewares/adminOrOwner.middleware";

const router = Router();

export const todoRouter = () => {
    router.post('', isAuthenticated, adminOrOwner, create);
    router.get('', isAuthenticated, isAdmin, list);
    router.get('/:id', isAuthenticated, adminOrOwner, findTodo);
    router.patch('/:id', isAuthenticated, adminOrOwner, update);
    router.delete('/:id', isAuthenticated, adminOrOwner, destroy)
    return router;
}
