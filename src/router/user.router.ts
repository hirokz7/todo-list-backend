import { Router } from "express";

import { create, list, currentUser, update, destroy } from "../controllers/user.controller";
import { isAuthenticated } from '../middlewares/authentication.middleware'
import { isAdmin } from "../middlewares/isAdmin.middleware";
import { adminOrOwner } from "../middlewares/adminOrOwner.middleware";

const router = Router();

export const userRouter = () => {
    router.post('', create);
    router.get('', isAuthenticated, isAdmin, list);
    router.get('/profile', isAuthenticated, currentUser);
    router.patch('/:id', isAuthenticated, adminOrOwner, update);
    router.delete('/:id', isAuthenticated, adminOrOwner, destroy)
    return router;
}
