import { getRepository, getCustomRepository } from "typeorm";
import { Request } from "express";
import { Todo, User } from "../entities";
import AppError from "../errors/AppError";


export const deleteTodos = async (req: Request, id: string) => {
    try{
        const userRepository = getRepository(User);
        const user = await userRepository.findOne({
            where:{
                id: id
            }, relations:['todolist']
        });
        const todoRepository = getRepository(Todo);
        
        await user?.todolist.map((item: any) =>{
            if(!item.isConcluded){
                 todoRepository.delete(item.id)
            }
        })

        return {}
           
    } catch (error){
        throw new AppError("Unauthorized!", 401);
    }
}


export const deleteDones = async (req: Request, id: string) => {
    try{
        const userRepository = getRepository(User);
        const user = await userRepository.findOne({
            where:{
                id: id
            }, relations:['todolist']
        });
        const todoRepository = getRepository(Todo);
        
        await user?.todolist.map((item: any) =>{
            if(item.isConcluded){
                 todoRepository.delete(item.id)
            }
        })

        return {}
           
           
    } catch (error){
        throw new AppError("Unauthorized!", 401);
    }
}


