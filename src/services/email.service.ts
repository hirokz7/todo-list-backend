import nodemailer from 'nodemailer'
import path from 'path';
import hbs, {NodemailerExpressHandlebarsOptions} from 'nodemailer-express-handlebars'
import { NextFunction, Request } from 'express'
import AppError from "../errors/AppError";


var transport = nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "7d8407713a1f74",
      pass: "94a7c6dedc87f7"
    }
});


const handlebarOption: NodemailerExpressHandlebarsOptions = {
    viewEngine: {
        partialsDir: path.resolve(__dirname, '..', 'templates'),
        defaultLayout: undefined
    },
    viewPath: path.resolve(__dirname, '..', 'templates')
}

transport.use('compile', hbs(handlebarOption));

export const mailTemplateOptions = (to: string, subject: string, template: string, context: any) => {
    return {
        from: 'not-reply@email.com',
        to,
        subject,
        template,
        context
    };
}

export const emailFromUser = async (req: Request, next: NextFunction) => {
    try{
        const { name, email, phone, message } = req.body


        const options = mailTemplateOptions(
            email,
            'User in touch!',
            'email',
            {
                name: name,
                message: message,
                phone: phone
            }
        );


        transport.sendMail(options, function (error, info) {
            if (error) {
                next(error)
            } else {
                console.log(info);
            }
        });

        return { message: 'Email sent' }        
    } catch (error){
        throw new AppError('User Not Found!', 404);
    }
}