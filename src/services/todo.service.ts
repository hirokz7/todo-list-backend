import { getRepository, getCustomRepository } from "typeorm";
import { Request } from "express";
import { Todo, User } from "../entities";
import AppError from "../errors/AppError";
import TodoRepository from '../repositories/todoRepository';


export const createTodo = async (req: Request) => {
    try{
        const { name } = req.body;

        if(!name){
            throw new AppError("Wrong fields or missing field!", 400);
        }
        const todoRepository = getRepository(Todo);
        const userRepository = getRepository(User)
        const user = await userRepository.findOne({
            where:{
                id: req.user.userData.id
            },
            relations:['todolist']
        })
        const todo = todoRepository.create({
            name
        });

        await todoRepository.save(todo);
        
        const newTodo = await todoRepository.findOne({
            where:{
                name: name
            }
        })

        if(user){
            user?.todolist.push(newTodo as any)
            await userRepository.save(user)
        }
        
        return todo;
    } catch (error) {
        throw new AppError("Wrong fields or missing field!", 400);
    }
    
}

export const listTodos = async () => {
    const todoRepository = getCustomRepository(TodoRepository);

    const todos = await todoRepository.find();

    return todos;
}


export const findId = async (id: string) => {
    try{
        const todoRepository = getCustomRepository(TodoRepository);
    
        const todo = await todoRepository.findById(id);

        if(!todo){
            throw new AppError("To-do Not Found", 404);
        }
    
        return todo;
    } catch (error) {
        throw new AppError("To-do Not Found", 404);
    }
}


export const updateTodo = async (req: Request, todoId: string) => {
    try{
        const todo_data = req.body;
        const todoRepository = getRepository(Todo);
        const todo = await todoRepository.findOne(todoId);

        if(!todo){
            throw new AppError("Wrong fields or missing field!", 400);
        }

        todo.updatedAt = new Date()

        await todoRepository.update(todoId, todo_data);

        const updatedTodo = await todoRepository.findOne(todoId);

        
        return updatedTodo
    
    } catch (error){
        throw new AppError("Wrong fields or missing field!", 400);
    }
}


export const deleteTodo = async (req: Request, todoId: string) => {
    try{
        const todoRepository = getRepository(Todo);
        const todo = await todoRepository.findOne(todoId);
        
        await todoRepository.delete(todoId)

        return {}
           
    } catch (error){
        throw new AppError("Unauthorized!", 401);
    }
}

