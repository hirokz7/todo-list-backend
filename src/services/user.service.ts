import { getRepository, getCustomRepository } from "typeorm";
import { Request } from "express";
import { User } from "../entities";
import AppError from "../errors/AppError";
import UserRepository from '../repositories/userRepository';
import bcrypt from 'bcrypt';
interface UserBody {
    name: string;
    email: string;
    password: string;
    isAdm: boolean;
}

export const createUser = async (body: UserBody) => {
    try{
        const { email, password, name, isAdm } = body;

        const userRepository = getRepository(User);

        const user = userRepository.create({
            name,
            email,
            password,
            isAdm,
        });


        await userRepository.save(user);


        return user;
    } catch (error) {
        throw new AppError("E-mail already registered", 400);
    }
    
}

export const listUser = async () => {
    const userRepository = getCustomRepository(UserRepository);

    const users = await userRepository.find();

    return users;
}

export const updateUser = async (req: Request, userId: string) => {
    try{
        const user_data = req.body;
        const userRepository = getRepository(User);
        const user = await userRepository.findOne(userId);

        if(!user){
            throw new AppError("User Not Found!", 404);
        }

        const { isAdm: user_isAdmin, ...bodyWithoutIsAdmin } = user_data;
        
        if (user_data.password) {
            const passwordHash = await bcrypt.hash(user_data.password, 10);
            bodyWithoutIsAdmin.password = passwordHash;
        }

        user.updatedAt = new Date()

        await userRepository.update(userId, bodyWithoutIsAdmin);

        const updatedUser = await userRepository.findOne(userId);

        const { password: user_password, ... userWithoutPassword }: any = updatedUser

        return userWithoutPassword
    
    } catch (error){
        throw new AppError("User Not Found", 404);
    }
}


export const getProfile = async (userId: string) => {
    try{
        const userRepository = getRepository(User);
        const user = await userRepository.findOne({
            where:{
                id: userId
            },
            relations:['todolist']     
        });

        if(!user){
            throw new AppError("User not Found!", 404);
        }
        
        const { password: user_password, ... userWithoutPassword } = user

        return userWithoutPassword
           
    } catch (error){
        throw new AppError("Missing admin permissions", 401);
}
}

export const deleteUser = async (req: Request, userId: string) => {
    try{
        const userRepository = getRepository(User);
        const user = await userRepository.findOne(userId);

        if(!user){
            throw new AppError("User not Found!", 404);
        }
        
        await userRepository.delete(userId)

        return { message: "User deleted with success" }
           
    } catch (error){
        throw new AppError("User not Found!", 404);
    }
}